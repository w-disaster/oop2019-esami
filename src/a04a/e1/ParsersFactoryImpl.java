package a04a.e1;

import java.util.ArrayList;
import java.util.List;

public class ParsersFactoryImpl implements ParsersFactory {

	@Override
	public Parser<Integer> createSequenceParserToCount(String t) {
		return new Parser<Integer>() {

			boolean isBroken = false;
			int result = 0;
			
			@Override
			public boolean getNext(String token) {
				if(this.isBroken == false) {
					if(token.equals(t)) {
						this.result = this.result + 1;
						return true;
					} else {
						this.isBroken = true;
						return false;
					}
				}
				return false;
			}

			@Override
			public boolean getAllInList(List<String> tokens) {
				for(String s : tokens) {
					if(!getNext(s)) {
						return false;
					}
				}
				return true;
			}

			@Override
			public Integer completeAndCreateResult() {
				if(this.isBroken == true) {
					throw new IllegalStateException();
				}
				return this.result;
			}
			
		};
	}

	@Override
	public Parser<String> createNonEmptySequenceParserToString(String t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Parser<Integer> createExpressionParserToResult() {
		return new Parser<Integer>() {

			private List<String> acceptableTokens = List.of("0", "1", "-", "+");
			private List<String> tokens = new ArrayList<>();
			private boolean isBroken = false;
			
			@Override
			public boolean getNext(String token) {
				String last = tokens.get(tokens.size());
				if(this.isBroken == false) {
					if(last.equals("0") || last.equals("1")) {
						if(token.equals("-") || token.equals("+")) {
							this.tokens.add(token);
							return true;
						}
						this.isBroken = true;
						return false;
					} else {
						if(last.equals("-") || last.equals("+")) {
							if(token.equals("0") || token.equals("1")) {
								this.tokens.add(token);
								return true;
							}
							this.isBroken = true;
							return false;
						}
					}
				}
				return false;
			}

			@Override
			public boolean getAllInList(List<String> tokens) {
				for(String s : tokens) {
					if(!this.getNext(s)) {
						return false;
					}
				}
				return true;
			}

			@Override
			public Integer completeAndCreateResult() {
				return null;
			}
			
		};
	}

}
