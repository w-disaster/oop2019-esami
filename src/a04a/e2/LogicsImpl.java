package a04a.e2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogicsImpl implements Logics{

	private List<Pair<Integer, Integer>> coordinates;
	private Integer size;
	
	public LogicsImpl(int size) {
		this.coordinates = new ArrayList<>();
		this.size = size;
	}
	
	@Override
	public List<Pair<Integer, Integer>> generatePositions() {
		if(this.coordinates.isEmpty()) {
			this.coordinates = Stream.generate(() -> new Pair<Integer, Integer>(new Random().nextInt(size - 1), new Random().nextInt(size - 1)))
					.limit(3)
					.collect(Collectors.toList());
		} else { 
			this.coordinates = this.coordinates.stream()
					.flatMap(p -> Stream.iterate(new Pair<>(p.getX() - 1, p.getY() - 1), c ->  new Pair<>(c.getX() + 1 , c.getY()))
							.limit(3)
							.flatMap(p1 -> Stream.iterate(p1, p2 -> new Pair<>(p2.getX(), p2.getY() + 1)).limit(3)))
					.filter(p -> p.getX() >=0 && p.getY() >=0 && p.getX() < this.size  && p.getY() < this.size )
					.collect(Collectors.toList());
		}
		return this.coordinates;
	}

	@Override
	public Boolean end() {
		List<Pair<Integer, Integer>> list = Stream.iterate(new Pair<>(0, 0), p -> new Pair<>(p.getX() + 1, p.getY()))
		.limit(this.size - 1)
		.flatMap(p -> Stream.iterate(p, c -> new Pair<Integer, Integer>(c.getX(), c.getY() + 1)).limit(this.size - 1))
		.collect(Collectors.toList());
		for(Pair<Integer, Integer> p : list) {
			if(!this.coordinates.contains(p)) {
				return false;
			}
		}
		return true;
	}


}
