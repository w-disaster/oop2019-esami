package a04a.e2;

import java.util.List;

public interface Logics {
	
	List<Pair<Integer, Integer>> generatePositions();
	
	Boolean end();


}
