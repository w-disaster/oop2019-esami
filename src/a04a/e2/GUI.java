package a04a.e2;

import javax.swing.*;
import java.util.*;
import java.util.List;
import java.util.Map.Entry;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    
	private Map<JButton, Pair<Integer, Integer>> buttons;
	private Logics logics;
	private JButton south;
    
    public GUI(int size) {
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500, 500);
        
        JPanel panel = new JPanel(new GridLayout(size, size));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        this.buttons = new HashMap<>();
        this.logics = new LogicsImpl(size);
        this.south = new JButton(">");
        
        this.getContentPane().add(BorderLayout.SOUTH, this.south);
        
        for(int i = 0; i < size; i++) {
        	for(int k = 0; k < size; k ++) {
        		JButton jb = new JButton("");
        		panel.add(jb);
        		this.buttons.put(jb, new Pair<>(i, k));
        	}
        }
        
        this.nextAnimation(this.buttons, this.logics);
        
        this.south.addActionListener(l -> {
        	this.nextAnimation(buttons, logics);
        });
        
        this.setVisible(true);
    }
    
    private void nextAnimation(Map<JButton, Pair<Integer, Integer>> map, Logics logics) {
    	List<Pair<Integer, Integer>> positions = logics.generatePositions();	
    	for(Entry<JButton, Pair<Integer, Integer>> e : buttons.entrySet()) {
    		if(positions.contains(e.getValue())) {
    			e.getKey().setText("*");
    		}
    	}
    	if(logics.end()) {
    		System.exit(0);
    	}
    }
    
}
