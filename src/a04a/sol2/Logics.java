package a04a.sol2;

import java.util.Set;

public interface Logics{
	
	void tick();
	
	boolean isOver();
	
	Set<Pair<Integer,Integer>> getPositions();
    
}
