package a01b.e2;

import java.util.Optional;

public interface Logics {
	
	public enum State {
		WALKABLE, HIT
	}
	
	Pair<Boolean, Optional<Integer>> hit(Pair<Integer, Integer> coordinate);
	
	boolean isWin();

}
