package a01b.e2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogicsImpl implements Logics {

	private Map<Pair<Integer, Integer>, State> table;
	private List<Pair<Integer, Integer>> mines;
	
	public LogicsImpl(int size, int mines) {
		super();
		this.table = new HashMap<>();
		for(int i = 0; i < size; i++) {
			for(int k = 0; k < size; k++) {
				this.table.put(new Pair<>(i, k), State.WALKABLE);
			}
		}
		this.mines = Stream.generate(() -> new Pair<Integer, Integer>(new Random().nextInt(size), new Random().nextInt(size)))
				.distinct()
				.limit(mines)
				.collect(Collectors.toList());	
	}

	@Override
	public Pair<Boolean, Optional<Integer>> hit(Pair<Integer, Integer> coordinate) {
		this.table.replace(coordinate, State.HIT);
		return this.mines.contains(coordinate) ? new Pair<>(true, Optional.empty()) : 
			new Pair<>(false, Optional.of(this.mines.stream()
					.filter(m -> Math.abs(m.getX() - coordinate.getX()) <= 1 && Math.abs(m.getY() - coordinate.getY()) <= 1)
					.mapToInt(i -> 1)
					.sum()));
	}

	@Override
	public boolean isWin() {
		return this.table.entrySet().stream()
				.filter(c -> c.getValue().equals(State.HIT) && !this.mines.contains(c.getKey()))
				.count() == this.table.size() - this.mines.size();
				
	}

}
