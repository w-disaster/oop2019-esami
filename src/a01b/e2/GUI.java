package a01b.e2;

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
     
	private Map<JButton, Pair<Integer, Integer>> buttons;
	private Logics logics;
	
    public GUI(int size, int mines) {
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500, 500);
        
        JPanel panel = new JPanel(new GridLayout(size, size));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        this.buttons = new HashMap<>();
        this.logics = new LogicsImpl(size, mines);
        
        for (int i = 0; i < size; i++){
        	for(int k = 0; k < size; k++) {
        		JButton jb = new JButton("");
        		panel.add(jb);
        		this.buttons.put(jb, new Pair<>(i, k));
        		jb.addActionListener(l -> {
        			Pair<Boolean, Optional<Integer>> hit = this.logics.hit(this.buttons.get(jb));
        			if(hit.getX()) {
        				System.out.println("Game over!");
        				System.exit(0);
        			}
        			if(this.logics.isWin()) {
        				System.out.println("You win!");
        				System.exit(0);
        			}
        			jb.setEnabled(false);
        			jb.setText(hit.getY().get().toString());
        		});
        	}   
        } 
        this.setVisible(true);
    }
    
    
}
