package a01b.e1;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GridFactoryImpl implements GridFactory {

	@Override
	public <E> Grid<E> create(int rows, int cols) {
		return new Grid<E>() {

			private List<Cell<E>> grid = Stream.iterate(new Cell<E>(0, 0, null), c -> new Cell<E>(c.getRow() + 1, 0, null))
					.limit(rows)
					.flatMap(r -> Stream.iterate(r , c -> new Cell<E>(c.getRow(), c.getColumn() + 1, null)).limit(cols))
					.collect(Collectors.toList());
			
			@Override
			public int getRows() {
				return rows;
			}

			@Override
			public int getColumns() {
				return cols;
			}

			@Override
			public E getValue(int row, int column) {
				return this.grid.stream()
						.filter(c -> c.getRow() == row && c.getColumn() == column)
						.limit(1)
						.map(c -> c.getValue())
						.collect(Collectors.toList())
						.get(0);
			}

			@Override
			public void setColumn(int column, E value) {
				for(Cell<E> c : this.grid) {
					this.grid.set(this.grid.indexOf(c), 
						c.getColumn() == column ? new Cell<E>(c.getRow(), c.getColumn(), value) : c);
				}
			}

			@Override
			public void setRow(int row, E value) {
				for(Cell<E> c : this.grid) {
					this.grid.set(this.grid.indexOf(c), 
						c.getRow() == row ? new Cell<E>(c.getRow(), c.getColumn(), value) : c);
				}
			}

			@Override
			public void setBorder(E value) {
				this.setRow(0, value);
				this.setColumn(0, value);
				this.setRow(rows - 1, value);
				this.setColumn(cols - 1, value);
			}

			@Override
			public void setDiagonal(E value) {
				for(Cell<E> c : this.grid) {
					this.grid.set(this.grid.indexOf(c), 
						c.getRow() == c.getColumn() ? new Cell<E>(c.getRow(), c.getColumn(), value) : c);
				}
			}

			@Override
			public Iterator<Cell<E>> iterator(boolean onlyNonNull) {
				return this.grid.stream()
						.filter(c -> onlyNonNull ? c.getValue() != null : true)
						.collect(Collectors.toList())
						.iterator();
			}
			
		};
	}

}
