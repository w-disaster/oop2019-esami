package a04b.sol2;

public interface Logics{
	
	void tick();
	
	boolean isOver();
	
	Pair<Integer,Integer> getPosition();
    
}
