package a02b.e2;

import java.util.List;

public interface Logics {

	/**
	 * adds the cell to the List
	 * @param pos
	 */
	void hit(Pair<Integer, Integer> pos);
	
	/**
	 * moves all the position up or down one position 
	 */
	void moveAll();
	
	/**
	 * @param pos
	 * @return true if pos contains a X, false otherwise
	 */
	Boolean containsX(Pair<Integer, Integer> pos);
	
	/**
	 * 
	 * @param posX
	 */
	void changeDirection(Integer posX);
}
