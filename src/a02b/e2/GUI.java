package a02b.e2;

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    
    private static final long serialVersionUID = -6218820567019985015L;
    private Map<Pair<Integer, Integer>, JButton> bMap;
    private JButton moveButton;
    private Logics logics;
    
    public GUI(int size) {
    	 this.setDefaultCloseOperation(EXIT_ON_CLOSE);
         this.setSize(500, 500);
         
         JPanel panel = new JPanel(new GridLayout(size,size));
         this.moveButton = new JButton(">");
         this.getContentPane().add(BorderLayout.CENTER,panel);
         this.getContentPane().add(BorderLayout.SOUTH, this.moveButton);
         
         this.logics = new LogicsImpl(size);
         this.bMap = new HashMap<>();
         for(int i = 0; i < size; i++) {
        	 for(int k = 0; k < size; k++) {
        		 JButton nb = new JButton("");
        		 this.bMap.put(new Pair<>(i, k), nb);
        		 panel.add(nb);
        	 }
         }
         
         this.bMap.entrySet().forEach(e -> {
        	 e.getValue().addActionListener(l -> {
        		e.getValue().setText("X");
        		logics.hit(e.getKey()); 
        	 });
         });
         
         this.moveButton.addActionListener(l -> {
        	 logics.moveAll();
        	 this.bMap.entrySet().forEach(e -> {
        		 e.getValue().setText(logics.containsX(e.getKey()) ? "X" : "");
        	 });
         });
         
         this.setVisible(true);
    }
    
}
