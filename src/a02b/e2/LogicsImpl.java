package a02b.e2;

import java.util.ArrayList;
import java.util.List;

public class LogicsImpl implements Logics{

	private List<Pair<Integer, Integer>> list;
	private Boolean direction;
	private Integer size;
	
	public LogicsImpl(Integer size) {
		this.list = new ArrayList<>();
		this.direction = true;
		this.size = size;
	}

	@Override
	public void hit(Pair<Integer, Integer> pos) {
		if(!this.list.contains(pos)) {
			this.list.add(pos);
		}
	}

	@Override
	public void moveAll() {
		this.list.forEach(p -> {
			this.changeDirection(p.getX());
			this.list.set(this.list.indexOf(p), new Pair<>(p.getX() + (this.direction ? 1 : -1), p.getY()));
		});
	}

	
	
	@Override
	public Boolean containsX(Pair<Integer, Integer> pos) {
		return this.list.contains(pos);
	}

	@Override
	public void changeDirection(Integer posX) {
		if(posX.equals(this.size)) {
			this.direction = false;
		} else if(posX.equals(0)) {
			this.direction = true;
		}
	}


}
