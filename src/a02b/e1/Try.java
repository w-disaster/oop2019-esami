package a02b.e1;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Try implements ExamsFactory {

	@Override
	public CourseExam<SimpleExamActivities> simpleExam() {
		
		return new CourseExam<SimpleExamActivities>() {

			Optional<SimpleExamActivities> la = Optional.empty(); 

			@Override
			public Set<SimpleExamActivities> activities() {
				Set<SimpleExamActivities> retSet = new HashSet<>();
				for(SimpleExamActivities sea : ExamsFactory.SimpleExamActivities.values()) {
					retSet.add(sea);
				}
				return retSet;
			}

			@Override
			public Set<SimpleExamActivities> getPendingActivities() {
				return this.activities().stream()
						.filter(sa -> sa.ordinal() == (la.isEmpty() ? 0 : la.get().ordinal() + 1))
						.collect(Collectors.toSet());
			}

			@Override
			public void completed(SimpleExamActivities a) {
				la = Optional.of(a);
			}

			@Override
			public boolean examOver() {
				return getPendingActivities().isEmpty();
			}
			
		};
	}

	@Override
	public CourseExam<OOPExamActivities> simpleOopExam() {
		
		
		
		return new CourseExam<OOPExamActivities>() {

			List<Pair<OOPExamActivities, Integer>> exActList;
			
			@Override
			public Set<OOPExamActivities> activities() {
				Set<OOPExamActivities> retSet = new HashSet<>();
				for(OOPExamActivities soe : ExamsFactory.OOPExamActivities.values()) {
					retSet.add(soe);
				}
				return retSet;
			}

			@Override
			public Set<OOPExamActivities> getPendingActivities() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void completed(OOPExamActivities a) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean examOver() {
				// TODO Auto-generated method stub
				return false;
			}
			
		};
	}

	@Override
	public CourseExam<OOPExamActivities> fullOopExam() {
		// TODO Auto-generated method stub
		return null;
	}

}
