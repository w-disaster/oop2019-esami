package a02b.e1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static a02b.e1.ExamsFactory.SimpleExamActivities.*;
import static a02b.e1.ExamsFactory.OOPExamActivities.*;

public class ExamsFactoryImpl implements ExamsFactory{

	<X> CourseExam<X> buildExam(Map<X, Pair<List<X>, Boolean>> map){
		return new CourseExam<X>() {

			//Map<X, Pair<List<X>, Boolean>> exams = new HashMap<>();
			
			
			@Override
			public Set<X> activities() {
				return map.keySet();
			}

			@Override
			public Set<X> getPendingActivities() {
				Set<X> set =  map.entrySet().stream()
						.filter(e -> {
							if(e.getValue().getY().equals(false)){
								for(X x : e.getValue().getX()) {
									if(map.get(x).getY().equals(false)) {
										return false;
									}
								}
								return true;
							}
							return false;
						})
						.map(e -> e.getKey())
						.collect(Collectors.toSet());
				set.forEach(e -> System.out.println(e));
				System.out.println("");
				return set;
			}

			@Override
			public void completed(X a) {
				map.replace(a, new Pair<>(map.get(a).getX(), true));
			}

			@Override
			public boolean examOver() {
				for(X x : this.activities()) {
					if(!map.get(x).getY()) {
						return false;
					}
				}
				return true;
			}
			
		};
	}

	@Override
	public CourseExam<SimpleExamActivities> simpleExam() {
		Map<SimpleExamActivities, Pair<List<SimpleExamActivities>, Boolean>> simpleExMap = new HashMap<>();
		simpleExMap.put(WRITTEN, new Pair<>(List.of(), false));
		simpleExMap.put(ORAL, new Pair<>(List.of(WRITTEN), false));
		simpleExMap.put(REGISTER, new Pair<>(List.of(WRITTEN, ORAL), false));
		return this.buildExam(simpleExMap);
	}

	@Override
	public CourseExam<OOPExamActivities> simpleOopExam() {
		Map<OOPExamActivities, Pair<List<OOPExamActivities>, Boolean>> simpleOOPExMap = new HashMap<>();
		simpleOOPExMap.put(LAB_REGISTER, new Pair<>(List.of(), false));
		simpleOOPExMap.put(LAB_EXAM, new Pair<>(List.of(LAB_REGISTER), false));
		simpleOOPExMap.put(LAB_EXAM, new Pair<>(List.of(LAB_REGISTER), false));
		simpleOOPExMap.put(PROJ_PROPOSE, new Pair<>(List.of(), false));
		simpleOOPExMap.put(PROJ_SUBMIT, new Pair<>(List.of(PROJ_PROPOSE), false));
		simpleOOPExMap.put(PROJ_EXAM, new Pair<>(List.of(PROJ_PROPOSE, PROJ_SUBMIT), false));
		simpleOOPExMap.put(FINAL_EVALUATION, new Pair<>(List.of(LAB_REGISTER, LAB_EXAM, PROJ_PROPOSE, PROJ_SUBMIT, PROJ_EXAM), false));
		return this.buildExam(simpleOOPExMap);
	}

	@Override
	public CourseExam<OOPExamActivities> fullOopExam() {
		Map<OOPExamActivities, Pair<List<OOPExamActivities>, Boolean>> OOPExMap = new HashMap<>();
		OOPExMap.put(STUDY, new Pair<>(List.of(), false));
		OOPExMap.put(LAB_REGISTER, new Pair<>(List.of(STUDY), false));
		OOPExMap.put(LAB_EXAM, new Pair<>(List.of(STUDY, LAB_REGISTER), false));
		OOPExMap.put(PROJ_PROPOSE, new Pair<>(List.of(STUDY), false));
		OOPExMap.put(PROJ_SUBMIT, new Pair<>(List.of(STUDY,PROJ_PROPOSE), false));
		OOPExMap.put(CSHARP_TASK, new Pair<>(List.of(STUDY, PROJ_PROPOSE, PROJ_SUBMIT), false));
		OOPExMap.put(PROJ_EXAM, new Pair<>(List.of(STUDY, PROJ_PROPOSE, PROJ_SUBMIT), false));
		OOPExMap.put(FINAL_EVALUATION, new Pair<>(List.of(STUDY, LAB_REGISTER, LAB_EXAM, PROJ_PROPOSE, PROJ_SUBMIT, CSHARP_TASK, PROJ_EXAM), false));
		return this.buildExam(OOPExMap);
	}
	
	
	
}
