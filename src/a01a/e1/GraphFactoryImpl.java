package a01a.e1;


import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GraphFactoryImpl<X> implements GraphFactory {
	
	private static <X> Graph<X> graphFromEdges(Set<Pair<X, X>> edges) {
		return new Graph<X>() {

			Set<Pair<X, X>> graphEdges = edges;
			
			@Override
			public Set<X> getNodes() {
				return graphEdges.stream()
						.flatMap(p -> Stream.of(p.getX(), p.getY()))
						.distinct()
						.collect(Collectors.toSet());
			}

			@Override
			public boolean edgePresent(X start, X end) {
				return graphEdges.contains(new Pair<X, X>(start, end));
			}

			@Override
			public int getEdgesCount() {
				return graphEdges.size();
			}

			@Override
			public Stream<Pair<X, X>> getEdgesStream() {
				return graphEdges.stream();
			}
			
		};
	}
	
	@Override
	public <X> Graph<X> createDirectedChain(List<X> nodes) {
		return graphFromEdges(nodes.stream()
		.map(n -> new Pair<X, X>(n, nodes.indexOf(n) + 1 == nodes.size() ? null : nodes.get(nodes.indexOf(n) + 1)))
		.limit(nodes.size() - 1).collect(Collectors.toSet()));
	}
	
	@Override
	public <X> Graph<X> createBidirectionalChain(List<X> nodes) {
		return graphFromEdges(nodes.stream()
		.map(n -> new Pair<X, X>(n, nodes.indexOf(n) + 1 == nodes.size() ? null : nodes.get(nodes.indexOf(n) + 1)))
		.flatMap(p -> Stream.of(p, new Pair<X, X>(p.getY(), p.getX())))
		.limit(nodes.size() * 2 - 2)
		.collect(Collectors.toSet()));
	}

	@Override
	public <X> Graph<X> createDirectedCircle(List<X> nodes) {
		return createDirectedChain(Stream.concat(nodes.stream(), Stream.of(nodes.get(0)))
				.collect(Collectors.toList()));
	}

	@Override
	public <X> Graph<X> createBidirectionalCircle(List<X> nodes) {
		return createBidirectionalChain(Stream.concat(nodes.stream(), Stream.of(nodes.get(0)))
				.collect(Collectors.toList()));
	}

	@Override
	public <X> Graph<X> createDirectedStar(X center, Set<X> nodes) {
		return graphFromEdges(nodes.stream()
				.map(n -> new Pair<X, X>(center, n))
				.collect(Collectors.toSet()));
	}

	@Override
	public <X> Graph<X> createBidirectionalStar(X center, Set<X> nodes) {
		return graphFromEdges(nodes.stream()
				.flatMap(n -> Stream.of(new Pair<X, X>(center, n), new Pair<X, X>(n, center)))
				.collect(Collectors.toSet()));
	}

	@Override
	public <X> Graph<X> createFull(Set<X> nodes) {
		return graphFromEdges(nodes.stream()
				.flatMap(n -> nodes.stream()
						.map(nn -> new Pair<X, X>(n, nn))
						.filter(nn -> !nn.equals(new Pair<X, X>(n, n))))
				.collect(Collectors.toSet()));
	}

	@Override
	public <X> Graph<X> combine(Graph<X> g1, Graph<X> g2) {
		return graphFromEdges(Stream.concat(g1.getEdgesStream(), g2.getEdgesStream())
				.collect(Collectors.toSet()));
	}

}
