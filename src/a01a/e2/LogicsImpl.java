package a01a.e2;

import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class LogicsImpl implements Logics {
	
	private Map<Pair<Integer, Integer>, State> boat;
	private Integer attempts;
	
	public LogicsImpl(int size, int boatSize) {
		super();
		Random r = new Random();
		this.attempts = 0;
		this.boat = Stream.iterate(Map.entry(new Pair<Integer, Integer>(r.nextInt(size - 1), r.nextInt(size - boatSize - 1)), State.ALIVE),
				e -> Map.entry(new Pair<Integer, Integer>(e.getKey().getX(), e.getKey().getY() + 1), e.getValue()))
		.limit(boatSize)
		.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
	}

	@Override
	public boolean hit(Pair<Integer, Integer> coordinate) {
		System.out.println(coordinate);
		this.attempts = this.attempts + 1;
		if(boat.containsKey(coordinate)) {
			boat.replace(coordinate, State.HIT);
			return true;
		}
		return false;
	}

	@Override
	public boolean isGameOver() {
		return this.attempts.equals(5);
	}

	@Override
	public boolean isWin() {
		return boat.entrySet().stream()
				.filter(e -> e.getValue().equals(State.HIT))
				.mapToInt(i -> 1)
				.sum() == boat.size();
	}
	
}
