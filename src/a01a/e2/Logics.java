package a01a.e2;

public interface Logics {
	
	public enum State {
		HIT, ALIVE
	}

	boolean hit(Pair<Integer, Integer> coordinate);
	
	boolean isGameOver();
	
	boolean isWin();
	
}
