package a01a.e2;

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    
	private Logics logics;
	private Map<JButton, Pair<Integer, Integer>> buttons;

    public GUI(int size, int boat) {        
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500, 500);
        JPanel panel = new JPanel(new GridLayout(size, size));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        this.logics = new LogicsImpl(size, boat);
        this.buttons = new HashMap<>();
        
        for(int i = 0; i < size; i++){
        	for(int k = 0; k < size; k++) {
        		final JButton jb = new JButton("");
        		buttons.put(jb, new Pair<Integer, Integer>(i, k));
            	jb.addActionListener(l -> {
            		jb.setText(this.logics.hit(this.buttons.get(jb)) ? "X" : "O");
            		if(this.logics.isWin()) {
            			System.out.println("You won!");
            			System.exit(0);
            		} else if(this.logics.isGameOver()) {
            			System.out.println("Game over!");
            			System.exit(0);
            		}
            	});
            panel.add(jb);
        	} 
        }
    	
    	this.setVisible(true);
    	
    }
    
}
