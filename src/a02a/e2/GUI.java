package a02a.e2;

import javax.swing.*;
import java.util.*;
import java.util.List;
import java.util.Map.Entry;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    
    private static final long serialVersionUID = -6218820567019985015L;
    
    private Map<JButton, Pair<Integer, Integer>> buttons;
    private JButton southButton;
    private Logics logics;
    
    public GUI(int size) {
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500, 500);
        
        JPanel panel = new JPanel(new GridLayout(size, size));
        this.buttons = new HashMap<>();
        this.southButton = new JButton(">");
        this.logics = new LogicsImpl(size);
        
        this.getContentPane().add(BorderLayout.CENTER,panel);
        this.getContentPane().add(BorderLayout.SOUTH, southButton);
                
        for(int i = 0; i < size; i++) {
        	for(int k = 0; k < size; k ++) {
        		JButton jb = new JButton("");
        		panel.add(jb);
        		this.buttons.put(jb, new Pair<>(i, k));
        		jb.addActionListener(l -> {
        			if(logics.end(this.buttons.get(jb))) {
        				System.exit(0);
        			}
        		});
        	}
        }
        
        southButton.addActionListener(l -> {
        	animation(this.buttons, this.logics);
        });
        
        this.animation(this.buttons, this.logics);
        this.setVisible(true);  
    }
    
    private void animation(Map<JButton, Pair<Integer, Integer>> buttons, Logics logics) {
    	List<Pair<Integer, Integer>> list = logics.nextAnimation();
    	for(Entry<JButton, Pair<Integer, Integer>> e : this.buttons.entrySet()) {
    		if(list.contains(e.getValue())) {
    			e.getKey().setText("X");
    		} else {
    			e.getKey().setText("");
    		}
    	}
    }
    
    
}
