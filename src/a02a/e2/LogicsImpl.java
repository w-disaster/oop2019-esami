package a02a.e2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogicsImpl implements Logics {

	private int size;
	private int border;
	Boolean direction;
	private List<Pair<Integer, Integer>> Xcoordinates;
	
	public LogicsImpl(int size) {
		this.size = size;
		this.border = -1;
		this.direction = true;
		this.Xcoordinates = new ArrayList<>();
	}
	
	@Override
	public List<Pair<Integer, Integer>> nextAnimation() {
		this.setBorder();
		this.Xcoordinates = Stream.iterate(new Pair<Integer, Integer>(this.border, this.border), p -> new Pair<>(p.getX() + 1, p.getY()))
				.limit(this.size - this.border)
				.flatMap(p -> Stream.iterate(p, i -> new Pair<>(i.getX(), i.getY() + 1)).limit(this.size - this.border))
				.filter(p -> p.getX().equals(this.border) || p.getX().equals(this.size/2) || p.getX().equals(this.size - 1 - border))
				.filter(p -> p.getY().equals(this.border) || p.getY().equals(this.size/2) || p.getY().equals(this.size - 1 - border))
				.filter(p -> !(p.getX().equals(this.size/2) && p.getY().equals(this.size/2)))
				.collect(Collectors.toList());
		
		if(this.Xcoordinates.size() == 0) {
			this.Xcoordinates.add(new Pair<>(size/2, size/2));
		}
		return this.Xcoordinates;
	}
	
	@Override
	public void setBorder() {
		this.border = this.direction.equals(true) ? this.border + 1 : this.border - 1;
		if(this.border == size/2) {
			changeDirection(false);
		}
		if(this.border == 0) {
			changeDirection(true);
		}
	}
	
	private void changeDirection(Boolean direction) {
		this.direction = direction;
	}
	
	@Override
	public Boolean end(Pair<Integer, Integer> coordinate) {
		return this.Xcoordinates.contains(coordinate);
	}


}
