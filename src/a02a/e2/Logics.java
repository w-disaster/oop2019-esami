package a02a.e2;

import java.util.List;

public interface Logics {
	
	 List<Pair<Integer, Integer>> nextAnimation();
	 
	 void setBorder();

	 Boolean end(Pair<Integer, Integer> coordinate);
	 
}
